# Bootstrap

* Used `ngx-bootstrap`, whose main (possibly only) difference is somewhere
  in the datepicker.
* https://valor-software.com/ngx-bootstrap/#/documentation#getting-started
* In order to get it to play nice with bootswatches, you have to update the
  angular.json
  * Outlined https://github.com/valor-software/ngx-bootstrap/blob/development/docs/getting-started/bootstrap.md#using-with-css
  * Basically point the 'styles' that normally points at vanilla bootstrap
    to the swatched version instead.
  * I kept it in assets but I don't know that it's actually needed there.
