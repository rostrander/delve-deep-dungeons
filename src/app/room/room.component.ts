import { Component, OnInit } from '@angular/core';
import {Character} from '../character/character';
import {Monster, MonsterGenerator} from '../monster/monster';
import {RollableDice} from '../dice-roll/dice';
import {Router} from '@angular/router';
import {DelveService} from '../delve.service';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit {

  level: number;

  encounter: Monster;

  constructor(
    private router: Router,
    public delve: DelveService,
  ) {

  }

  ngOnInit(): void {
    this.level = this.delve.level;
    this.encounter = null;

    this.delve.persist();
  }

  delveDeeper(): void {
    this.level += 1;
    this.delve.level = this.level;
    this.checkForEvent();
  }

  checkForEvent(): void {
    this.encounter = null;

    const eventRoll = RollableDice.roll(1, 12).value;
    if (eventRoll <= 9) {
      this.encounter = new MonsterGenerator().choose();
    }
  }

  startEncounter(): void {
    this.delve.encounter = this.encounter;
    this.router.navigate(['combat']);
  }
}
