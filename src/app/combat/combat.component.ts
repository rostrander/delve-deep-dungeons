import { Component, OnInit } from '@angular/core';
import {DelveService} from '../delve.service';
import {Character} from '../character/character';
import {Router} from '@angular/router';
import {RollableDice} from '../dice-roll/dice';

interface TranscriptEntry {
  message: string;
  dice?: RollableDice;
}

@Component({
  selector: 'app-combat',
  templateUrl: './combat.component.html',
  styleUrls: ['./combat.component.css']
})
export class CombatComponent implements OnInit {

  transcript: TranscriptEntry[];
  turn: number;
  pending: boolean;
  resolveFunc: (value?: void | PromiseLike<void>) => void;

  get turnIndex() {
    return this.turn % (this.delve.getParty().length + 1);
  }

  constructor(
    private router: Router,
    public delve: DelveService,
  ) {

  }

  ngOnInit(): void {
    this.transcript = [];
    this.turn = -1;
    this.pending = false;
    this.resolveFunc = null;
    this.script('Combat begins!');
    this.nextTurn();
  }

  nextTurn(): void {
    if (this.returnDeadOrAlive()) { return; }
    this.turn += 1;
    this.script(`Turn ${this.turn + 1}: ${this.currentTurnName()}`);
    if (this.isMonsterTurn()) {
      this.takeMonsterTurn();
    }
  }

  currentTurnName(): string {
    if (this.isMonsterTurn()) { return this.delve.encounter.name; }
    return this.currentPlayerTurn().name;
  }

  currentPlayerTurn(): Character {
    if (this.isMonsterTurn()) { return null; }
    return this.delve.getParty()[this.turnIndex];
  }

  isMonsterTurn(): boolean {
    return this.turnIndex >= this.delve.getParty().length;
  }

  script(message: string, dice?: RollableDice) {
    this.transcript.unshift({
      message,
      dice,
    });
  }

  visibleTranscript(lines = 5) {
    return this.transcript.slice(0, lines);
  }

  async attack() {
    const who = this.currentPlayerTurn();
    const monster = this.delve.encounter;
    const roll = who.attackRoll(monster);

    const hits = roll.hits(monster.level);
    if (hits <= 0) {
      this.script(`${who.name} misses!`, roll);
    } else {
      const times = hits > 1 ? ` ${hits} times` : '';
      this.script(`${who.name} hits${times}!`, roll);
      monster.damage(hits);
      if (monster.isDefeated()) {
        this.script(`The ${monster.name} dies!`);
      }
    }
    await this.pauseForAcknowledgement();
    this.nextTurn();
  }

  async takeMonsterTurn(monsterNumber = 1) {
    const monster = this.delve.encounter;
    let target = RollableDice.roll().value - 1;
    // Choosing between 1 and 6 so I can make the front two favored
    target = target % this.delve.getParty().length;
    // This is a kinda cheap way to do this (3 character parties lose all front
    // protection, for instance) but it'll do for now.
    const who = this.delve.getParty()[target];
    if (!who) {
      // Eventually we want to have smarter (re)targeting, but for now this will work
      this.script(`${monster.name} stands there with ${monster.name} incompetence`);
    } else {
      const roll = who.defenseRoll(monster);
      const wounds = roll.damages(monster.level);
      let verb = 'missed!';
      if (wounds > 0) {
        verb = `hit ${who.name} for ${wounds} damage!`;
        who.takeWound(wounds);
      }
      this.script(`${monster.aOrThe()} ${monster.name} ${verb}`, roll);
      if (who.isDefeated()) {
        this.script(`${who.name} dies!`);
      }
    }
    await this.pauseForAcknowledgement();
    if (monsterNumber >= monster.hp) {
      this.nextTurn();
    } else {
      return this.takeMonsterTurn(monsterNumber + 1);
    }
  }

  returnDeadOrAlive(): boolean {
    if (this.delve.partyIsDefeated()) {
      this.router.navigate(['game-over']);
      return true;
    }

    const monster = this.delve.encounter;
    if (!monster.isDefeated()) { return false; }

    this.router.navigate(['room']);
  }

  pauseForAcknowledgement(): Promise<void> {
    this.pending = true;
    const result = new Promise<void>((resolve) => {
      this.resolveFunc = resolve;
    });
    return result.then(() => { this.pending = false; } );
  }

  acknowledge(): void {
    this.resolveFunc();
  }
}
