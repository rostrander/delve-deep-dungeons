import {RollableDice} from '../dice-roll/dice';

export interface MonsterTemplate {
  name: string;
  level: number;
  amount?: RollableDice;
  isBoss?: boolean;
  hp?: number;
}

export class Monster implements MonsterTemplate {
  name: string;
  level: number;
  amount: RollableDice;
  isBoss: boolean;
  hp: number;

  constructor(template: MonsterTemplate) {
    Object.assign(this, template);
    if (this.amount === undefined) {
      this.amount = RollableDice.fixed(1);
    }
    if (this.hp === undefined) {
      this.hp = this.amount.value;
    }
  }

  public isDefeated(): boolean {
    return this.hp <= 0;
  }

  public damage(amount: number): void {
    this.hp = Math.max(0, this.hp - amount);
  }

  singularOrPluralName(): string {
    if (this.amount.value === 1) {
      return this.name;
    }
    return `${this.name}s`;
  }

  aOrThe(): string {
    if (this.amount.value === 1) { return 'the'; }
    return 'a';
  }
}

export class MonsterGenerator {
  private templates;

  constructor() {
    this.templates = [];
  }

  public choose(): Monster {
    return new Monster({
      name: 'Kobold',
      level: 3,
      amount: new RollableDice(1, 4),
    });
  }
}
