import {Component, Input, OnInit} from '@angular/core';
import {Monster} from './monster';

@Component({
  selector: 'app-monster',
  templateUrl: './monster.component.html',
  styleUrls: ['./monster.component.css']
})
export class MonsterComponent implements OnInit {

  @Input() item: Monster;

  constructor() { }

  ngOnInit(): void {
  }

}
