import {Component, Input, OnInit} from '@angular/core';
import {Character} from './character';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css']
})
export class CharacterComponent implements OnInit {

  @Input() item: Character;

  constructor() { }

  ngOnInit(): void {
  }

}
