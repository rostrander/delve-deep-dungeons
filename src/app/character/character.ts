import {Monster} from '../monster/monster';
import {RollableDice} from '../dice-roll/dice';
import {CharacterClass} from './character-class';

export interface CharacterTemplate {
  name: string;
  clsName: string;
  level?: number;
  wounds?: number;
}

export class Character implements CharacterTemplate {
  clsName: string;
  level: number;
  name: string;
  wounds: number;

  cls: CharacterClass;

  constructor(template: CharacterTemplate) {
    this.level = 1;
    Object.assign(this, template);
    if (this.wounds === undefined) {
      this.wounds = 0;
    }
    this.cls = CharacterClass.createForChar(this);
  }

  toJSON() {
    let result = {...this};
    delete result.cls;
    return result;
  }

  get hp(): number {
    return this.maxHp - this.wounds;
  }

  get maxHp(): number {
    return this.cls.maxHp;
  }

  attackRoll(enemy: Monster): RollableDice {
    const roll = RollableDice.roll();
    roll.addToRoll(this.cls.attackBonusVs(enemy), 'class');

    return roll;
  }

  defenseRoll(enemy: Monster): RollableDice {
    const roll = RollableDice.roll();
    roll.addToRoll(this.cls.defenseBonusVs(enemy), 'class');
    return roll;
  }

  takeWound(amount = 1): void {
    this.wounds += amount;
  }

  isDefeated(): boolean {
    return this.hp <= 0;
  }
}
