import {Character} from './character';
import {Monster} from '../monster/monster';

export enum CharClasses {
  Fighter = 'Fighter',
  Rogue = 'Rogue',
}

export abstract class CharacterClass {

  static createForChar(char: Character): CharacterClass {
    const clsName: CharClasses = char.clsName as CharClasses;
    switch (clsName) {
      case CharClasses.Fighter:
        return new Fighter(char);
      case CharClasses.Rogue:
        return new Rogue(char);
      default:
        return new NullCharacterClass(char);
    }
  }

  constructor(public char: Character) {

  }

  // Methods intended to be overridden by subclasses
  get maxHp(): number {
    return 1;
  }

  attackBonusVs(enemy: Monster): number {
    return 0;
  }

  defenseBonusVs(enemy: Monster): number {
    return 0;
  }
}

export class NullCharacterClass extends CharacterClass {

}

export class Fighter extends CharacterClass {

  attackBonusVs(enemy: Monster): number {
    return this.char.level;
  }

  get maxHp() {
    return 6 + this.char.level;
  }
}

export class Rogue extends CharacterClass {
  defenseBonusVs(enemy: Monster): number {
    return this.char.level;
  }

  get maxHp() {
    return 3 + this.char.level;
  }
}
