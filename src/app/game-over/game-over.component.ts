import { Component, OnInit } from '@angular/core';
import {DelveService} from '../delve.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-game-over',
  templateUrl: './game-over.component.html',
  styleUrls: ['./game-over.component.css']
})
export class GameOverComponent implements OnInit {

  constructor(
    public delve: DelveService,
    public router: Router,
  ) { }

  ngOnInit(): void {
  }

  restart() {
    this.delve.gameOver();
    this.router.navigate(['title']);
  }

}
