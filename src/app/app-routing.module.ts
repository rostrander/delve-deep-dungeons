import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {RoomComponent} from './room/room.component';
import {TitleComponent} from './title/title.component';
import {CombatComponent} from './combat/combat.component';
import {GameOverComponent} from './game-over/game-over.component';

const routes: Routes = [
  { path: 'combat', component: CombatComponent },
  { path: 'room', component: RoomComponent },
  { path: 'title', component: TitleComponent },
  { path: 'game-over', component: GameOverComponent },
  { path: '', redirectTo: '/title', pathMatch: 'full'},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule,
  ],
})
export class AppRoutingModule { }
