import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {DelveService, SavedCampaign, SaveGame} from '../delve.service';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent implements OnInit {

  constructor(
    public delve: DelveService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  get currentGame(): SaveGame {
    if (!this.delve.campaign) { return null; }
    return this.delve.campaign.currentGame;
  }

  continueGame() {
    this.delve.restore();
    this.router.navigate(['room']);
  }


  newGame() {
    this.delve.newGame();
    this.router.navigate(['room']);
  }

}
