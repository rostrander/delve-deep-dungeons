import { Injectable } from '@angular/core';
import {Character, CharacterTemplate} from './character/character';
import {Monster, MonsterGenerator} from './monster/monster';
import {CharacterClass, CharClasses} from './character/character-class';

export interface SavedCampaign {
  version: number;
  currentGame: SaveGame;
}

export interface SaveGame {
  partyTemplate: CharacterTemplate[];
  deepestLevel: number;
  currentLevel: number;
}

@Injectable({
  providedIn: 'root'
})
export class DelveService {

  public party: Character[];
  public encounter: Monster;
  public campaign: SavedCampaign;

  private deepestLevel: number;
  private currentLevel: number;

  constructor() {
    this.ensureLocalStorage();
    // DEBUG
    let w: any = window;
    w.DelveService = this;
  }

  get level(): number {
    return this.currentLevel;
  }

  set level(newLevel: number) {
    this.currentLevel = newLevel;
    if (this.currentLevel > this.deepestLevel) {
      this.deepestLevel = this.currentLevel;
    }
  }

  public getParty(): Character[] {
    return this.party;
  }

  public partyIsDefeated(): boolean {
    return this.party.every((char) => char.isDefeated());
  }

  // Savegame stuff

  public asJsonable(): SaveGame {
    return {
      deepestLevel: this.deepestLevel,
      currentLevel: this.currentLevel,
      partyTemplate: this.party,
    };
  }

  public newGame(): void {
    this.party = [
      new Character({name: 'Steve', clsName: CharClasses.Fighter}),
      new Character({name: 'Fred', clsName: CharClasses.Rogue}),
    ];
    this.deepestLevel = 1;
    this.currentLevel = 1;
    this.persist();
  }

  public gameOver(): void {
    // Kinda hacky, but for now it'll do:
    localStorage.removeItem('campaign');
    this.ensureLocalStorage();
  }

  // Actually save the game!
  public persist(): void {
    const game = this.asJsonable();
    this.campaign.currentGame = game;
    localStorage.setItem('campaign', JSON.stringify(this.campaign));
  }

  public restore(): void {
    this.ensureLocalStorage();
    if (!this.campaign.currentGame) { return this.newGame(); }

    const saved = this.campaign.currentGame;
    this.deepestLevel = saved.deepestLevel;
    this.currentLevel = saved.currentLevel;
    this.party = saved.partyTemplate.map( template => {
      return new Character(template);
    });
  }

  public ensureLocalStorage(): void {
    let campaignJson = localStorage.getItem('campaign');
    if (!campaignJson) {
      const campaign: SavedCampaign = {
        version: 1,
        currentGame: null
      };
      campaignJson = JSON.stringify(campaign);
      localStorage.setItem('campaign', JSON.stringify(campaign));
    }
    this.campaign = JSON.parse(campaignJson);
  }

}
