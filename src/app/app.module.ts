import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RoomComponent } from './room/room.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TitleComponent } from './title/title.component';
import { AppRoutingModule } from './app-routing.module';
import { CharacterComponent } from './character/character.component';
import { MonsterComponent } from './monster/monster.component';
import { CombatComponent } from './combat/combat.component';
import { GameOverComponent } from './game-over/game-over.component';
import { DiceRollComponent } from './dice-roll/dice-roll.component';

@NgModule({
  declarations: [
    AppComponent,
    RoomComponent,
    TitleComponent,
    CharacterComponent,
    MonsterComponent,
    CombatComponent,
    GameOverComponent,
    DiceRollComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
