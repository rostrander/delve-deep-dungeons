
interface RollAdditions {
  amount: number;
  reason: string;
  longReason?: string;
}

export class RollableDice {
  public rolledValue: number;
  public additions: RollAdditions[];

  public static fixed(value: number): RollableDice {
    return new RollableDice().fixValue(value);
  }

  public static roll(n= 1, d= 6, plus= 0): RollableDice {
    let tempDie = new RollableDice(n, d, plus);
    return tempDie;
  }

  constructor(public n = 1, public d = 6, public plus = 0) {
    this.additions = [];
  }

  public get alwaysFail(): boolean {
    return this.rolledValue === 1;
  }

  public get alwaysSucceed(): boolean {
    return this.rolledValue === 6;
  }

  public get hasAdditions(): boolean {
    return this.additions.length > 0;
  }

  public hits(target: number): number {
    if (this.alwaysFail) { return 0; }
    let hits = Math.floor(this.value / target);
    if (hits <= 0) {
      hits = 0;
      if (this.alwaysSucceed) { hits = 1; }
    }
    return hits;
  }

  public damages(target: number): number {
    if (this.alwaysFail) { return 1; }
    if (this.alwaysSucceed) { return 0; }
    return this.value > target ? 0 : 1;
  }

  public get value(): number {
    this.roll();
    let total = this.rolledValue;
    for (let added of this.additions) {
      total += added.amount;
    }
    return total;
  }

  public addToRoll(
    amount: number,
    reason: string,
    longReason = '',
    forceZeroAddition = false,
  ) {
    if (amount === 0 && !forceZeroAddition) { return; }
    const addition = {
      amount,
      reason,
      longReason
    };
    this.additions.push(addition);
  }

  public fixValue(value: number): this {
    this.rolledValue = value;
    return this;
  }

  public roll(): this {
    if (this.rolledValue === undefined) {
      let total = 0;
      for (let i = 0; i < this.n; i++) {
        total += Math.floor(Math.random() * this.d) + 1;
      }
      this.rolledValue = total + this.plus;
    }
    return this;
  }
}
