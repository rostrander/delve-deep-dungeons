import {Component, Input, OnInit} from '@angular/core';
import {RollableDice} from './dice';

@Component({
  selector: 'app-dice-roll',
  templateUrl: './dice-roll.component.html',
  styleUrls: ['./dice-roll.component.css']
})
export class DiceRollComponent implements OnInit {

  @Input() dice: RollableDice;

  constructor() { }

  ngOnInit(): void {
  }

}
